//
//  AppDelegate.swift
//  FITS
//
//  Created by Thiago Medeiros dos Santos on 12/07/2022.
//  Copyright (c) 2022 Thiago Medeiros dos Santos. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

